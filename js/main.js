var language = 'big5';

$(document).ready(function(){
	$(".home-pic").attr("src", $(".home-pic").attr("src"));
	$('#filer_input').filer({
	 	showThumbs: true,
		addMore: true,
		allowDuplicates: false
	});   

	$("input[name='pictures[]']").attr("name", "pictures");  

	$(".icon-en").click(function(){
		$(language).hide();
		if(language == 'big5'){
			$(this).attr("src", "img/ch-new.png");
			language = 'en';
		}
		else{
			$(this).attr("src", "img/h-en.png");
			language = 'big5';
		}
		$(language).show();
	});

	readXml();
	$(".open-nav").click(function(){
		$("nav").hide();
		$("nav").css("padding", "5vh 5px 5px 5px");
		$("nav .list-contain").css("display", "flex");
		$("nav .info").css("display", "block");
		$("nav").css("width", "150px");
		$(".open-nav").show();	
		$("nav").show("blind", {direction: "left"}, 1000, function(){
			$(".open-nav").hide();	
		});
		
	});
	

	$(".header-left").on("click",function(){
		$("section").hide();
		$("#homepage").fadeIn(500);
		$(".mobile-title").text("");
		location.replace("");
	});

	$(".list-contain li").on("click",function(){
		if($(window).width() < 801){
			$("nav").fadeOut(400);
		}
		$(".h-mobile").css("display", "flex");
		$("section").hide();
		$("#"+$(this).attr("class")).fadeIn(500);
		$(".mobile-title").html(convertTitle($(this).attr("class")));
		if(language == "big5"){
			$("big5").show();
			$("en").hide();
		}else{
			$("big5").hide();
			$("en").show();
		}
		location.assign("#"+$(this).attr("class"));
		$('html, body').scrollTop(0);

		$(".list-contain li").each(function(){
			$(this).find("div").css("width", "10px");
			$(this).find("div").css("opacity", "0");
		});
		$(this).find("div").css("width", "36px");
		$(this).find("div").css("opacity", "1");
	});

	$(".tickets").on("click",function(){
		$("section").hide();
		$('html, body').scrollTop(0);
		$("#"+$(this).attr("class")).fadeIn(500);
		location.assign("#"+$(this).attr("class"));
	});

	$(".mobile-fix").on("click",function(){
		$('html, body').scrollTop(0);
		$("section").hide();
		$("#tickets").fadeIn(500);
		$(".mobile-title").html(convertTitle($(this).attr("class")));
		if(language == "big5"){
			$("big5").show();
			$("en").hide();
		}else{
			$("big5").hide();
			$("en").show();
		}
		location.assign("#mobile-fix");
	});


	//偽裝router
	$(".list-contain li").each(function(){
		if(location.href.indexOf($(this).attr("class"))!==-1){
			$(this).trigger("click");
			if($(window).width() > 800){
				$(".open-nav").trigger("click");
			}
		}else if(location.href.indexOf("mobile-fix")!==-1){
			$(".mobile-fix").trigger("click");
		}else if(location.href.indexOf("tickets")!==-1){
			$(".tickets").trigger("click");
		}
		

	});


	$(".about-toggle-close").on("click", function(){
		$(".about-license").hide("slide", { direction: "right" }, 1000,function(){
			$("nav").fadeIn(1000);
			$(".h-pc").fadeIn(1000);
			$(".h-mobile").fadeIn(1000);	
		});
		
	});

	$(".about-toggle-open").on("click", function(){
		$(".about-license").show("slide", { direction: "right" }, 1000);
		$("nav").toggle();
		if($(window).width() < 801){
			$(".h-pc").hide();
			$(".h-mobile").hide();
		}
	});

	$(".about-toggle-mobile").on("click", function(){
		$(".about-license-mobile").show("slide", { direction: "down" }, 1000);
		$(".h-mobile").fadeOut();
	});

	$(".about-license-mobile-close").on("click", function(){
		$(".about-license-mobile").hide("slide", { direction: "down" }, 1000);
		$(".h-mobile").fadeIn();
		$(".h-mobile").css("display", "flex");
	});


	$(".about-light-box").on("click", function(){
		$(".about-light-box").fadeToggle();
		if($(window).width() > 800){
			$(".about-license").fadeToggle();
		}
	});

	$(".license-item").on("click", function(){
		$(".about-light-box .light-box-img").attr("src",
						  "img/about/"+$(this).attr("goto")+".png");
		$(".about-light-box").fadeToggle();
		
		if($(window).width() > 800){
			$(".about-license").fadeToggle();
		}
	});

    $(".team-down .team-item").mouseover(function(){
    	$(this).find(".item-dot").show();

    });

    $(".team-down .team-item").mouseout(function(){
    	$(this).find(".item-dot").hide();
    });

    $(".team-down .team-item").on("click", function(){
    	$(".team-down .team-item").find("div").removeClass("active");
    	$(this).find("div").addClass("active");
  		$(".team-upper .item-img").attr("src", "img/team/"+$(this).attr("id").substring(4)+".jpg"); 
  		$(".item-set .item-title").text($(this).find(".item-title").text());
  		$(".item-set .item-name").text($(this).find(".item-name").text());
  		$(".team-upper .item-detial ul").html($(this).find("ul").html());
    });

    $(".team-down .team-item").on("click", function(){
  		$(".team-upper").fadeIn();
  	});

    $(".team-upper").on("click", function(){
		if($(window).width() < 801){
			$(".team-upper").hide();
  			$(".h-mobile").css("display", "flex");
		}
  	});

    $(".mobile-menu").on("click",function(){
    	$("nav").fadeIn(500, function(){
    		$(".h-mobile").hide();	
    	});
    });

	$(".close-mobile-nav").on("click",function(){
	    $("nav").fadeOut(500);
		$(".h-mobile").css("display", "flex");
	});

	$(".collection-menu div").on("mouseenter", function(){
		//$(this).find('#col-title-bg').show();
	});

	$(".collection-menu div").on("mouseleave", function(){
		//$(this).find('#col-title-bg').hide();
	});

	$(".collection-menu div").on("click", function(){
		$(".collection-detail").fadeIn();
		$(".collection-menu").hide();
		$(".col-detial-title").html($(this).find("p").html());
		$(".project-item").hide();
		$(".project-item[type='"+$(this).find("p").attr("type")+"']").show();
		$(".project-item[type='"+$(this).find("p").attr("type")+"']").find("img").show();
		$(".project-dot").hide();
	});

	$(".collection-menu-mobile div").on("click", function(){
		$(".collection-detail").fadeIn();
		$(".collection-menu-mobile").hide();
		$(".col-detial-title").html($(this).find("p").html());
		$(".project-item").hide();
		$(".project-item[type='"+$(this).find("p").attr("type")+"']").show();
		$(".project-item[type='"+$(this).find("p").attr("type")+"']").find("img").show();
		$(".project-dot").hide();
	});

	$(".collection-detial-close").click(function(){
		$(".collection-detail").fadeOut();
		if($(window).width() > 800){
			$(".collection-menu").fadeIn();
		}else{
			$(".collection-menu-mobile").fadeIn();
		}
	});

	$(".collection-item-detial-close").click(function(){
		$(".collection-detail").show();
		$(".collection-item-detial").hide();
		$(".col-img-list-img").hide();
	});

	$(".alert-ok").click(function(){
		$(".fix-alert").hide();
	});

	$(".sex-btn").click(function(){
		$(".sex-btn").removeClass("active");
		$(this).addClass("active");
		$(".sex-input").attr("value",$(this).attr("value"));
	});

	$(".jFiler-input").html("<big5>選擇圖片</big5><en>select image</en>");

	$("#usrform").submit(function(){
		event.preventDefault();
		if($(".jFiler-item-others").length == 0) {
    		$(".error-msg").text("At least one picture.");
        	$(".alert-title").hide();
        	$(".error-msg").show();
            $(".fix-alert").css("display", "flex");
		}else if($(".jFiler-item-others").length > 10){
			$(".error-msg").text("At most ten pictures.");
	        $(".alert-title").hide();
	        $(".error-msg").show();
	        $(".fix-alert").css("display", "flex");
		}
		else{
			var formData = new FormData($(this)[0]);
			$.ajax({
	            type:'POST',
	            url: $(this).attr('action'),
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success:function(data){
	            	console.log(data);
	            	if(data.state){
	            		$(".alert-title").show();
		                $(".fix-count").text(data.data.no);
		                $(".fix-alert").show();
		                $(".error-msg").hide();
		                $(".fix-alert").css("display", "flex");
		                $('#usrform')[0].reset();
		                $("#filer_input").prop("jFiler").reset();
		            }else{
		            	$(".alert-title").hide();
		            	$(".error-msg").show();
		            	$(".error-msg").text(data.msg.msg+"請重新輸入!");
		                $(".fix-alert").show();
		                $(".fix-alert").css("display", "flex");
		            }
	            },
	            error: function(data){
	            	$(".error-msg").text("伺服器錯誤，請聯絡我們!");
	            	$(".alert-title").hide();
	            	$(".error-msg").show();
	                $(".fix-alert").css("display", "flex");
	            }
	        });
		}

	});

});


function preloadimg(){
	$.preload( 
	    'img/title.gif',
	  	'img/about/1.png',
	  	'img/about/2.png',
	  	'img/about/A.png',
	  	'img/about/B.png',
	  	'img/about/C.png',
	  	'img/about/D.png',
	  	'img/about/E.png',
	  	'img/about/F.png',
	  	'img/collection/1.png',
	  	'img/collection/2.png',
	  	'img/collection/3.png',
	  	'img/collection/4.png',
	  	'img/collection/5.png',
	  	'img/collection/m1.png',
	  	'img/collection/m2.png',
	  	'img/collection/m3.png',
	  	'img/collection/m4.png',
	  	'img/collection/m5.png'
	);
}


function convertTitle(className){
	if(className == "about")
		return "<big5>關於承甲</big5><en>About</en>";
	if(className == "portfolio")
		return "<big5>作品賞析</big5><en>Portfolio</en>";
	if(className == "clients")
		return "<big5>合作客戶</big5><en>Clients</en>";
	if(className == "team")
		return "<big5>設計團隊</big5><en>Studio</en>";
	if(className == "location")
		return "<big5>拜訪我們</big5><en>Location</en>";
	if(className == "board")
		return "<big5>留言板</big5><en>Board</en>";
	if(className == "tickets")
		return "<big5>線上報修</big5><en>Repairs</en>";
}

function readXml(){
	$.ajax({
	    url:'img/collection/projects/project-setting.xml',
	    type: 'GET',
	    dataType: 'xml',
	    timeout: 1000,
	    error: function(xml){
	        //當xml讀取失敗
	    },
	    success: function(xml){
	    	$(xml).find("project").each(function(){
	    		var scrStr = "background-image: url(img/collection/projects/"
	    						+ $(this).children("projectName").text()
	    						+"/1.png)";
	    		var projectName = $(this).children("projectName").text();
	    		$tempStr = $('<div class="project-item" ' 
	    						+ ' type="'
	    						+ $(this).children("type").text()
	    						+ '">'
	    						+ '<div class="p-item-img" style="'
	    						+  scrStr
	    						+ '" style="display: none;"></div>'
	    						+ '<p class="date" style="display: none;">'
	    						+ $(this).children("time").text()
	    						+ '</p><p class="name">'
	    						+ '<big5>'+projectName+'</big5>'
	    						+ '<en>'
	    						+ $(this).children("enName").text()
	    						+ '</en>'
	    						+ '</p></div>');
	    								// <img class="project-dot" src="img/more.png">
	    		$(".col-projects").append($tempStr);

	    		$(this).children("imgs").children("img").each(function(){
	    			var innerStyle = "display:none;";
	    			var innerAttr = "img/collection/projects/"
						    			+ projectName
						    			+"/"
						    			+ $(this).text();
	    			var tempInnerStr = '<div class="p-inner-container"><img src="'+innerAttr+'" class="col-img-list-img" company="'
	    								+ projectName
	    								+'" style="'
	    								+innerStyle
	    								+'"></div>';

	    			$(".col-img-list").append(tempInnerStr);
	    		});
	    	});
	    	
			$(".project-item").click(function(){
				if($(window).width() < 801){
					loadingPage();
				}
				var tempCompany = $(this).find(".name").html();
				$(".collection-detail").hide();
				$(".collection-item-detial").fadeIn();
				$(".item-detial-date").text($(this).find(".date").text());
				$(".item-detial-name").html(tempCompany);
				$(".col-img-list-img").hide();
				var companyName = $(this).find(".name").find("big5").text();
				$(".col-img-list-img[company="+companyName+"]").show();
				$(".col-detail-image").css("background-image","url("+ $(".col-img-list-img[company="+companyName+"]").first().attr("src")+ ")" );
			});

		    $(".project-item").mouseover(function(){
		    	$(this).find(".project-dot").show();
		    });

		    $(".project-item").mouseout(function(){
		    	$(this).find(".project-dot").hide();
		    });

			$(".col-img-list-img").click(function(){
				$(".col-detail-image").css("background-image","url("+ $(this).attr("src")+ ")" );
			});
		}
	});
}


function loadingPage(){
	$(".loading").css("display", "flex");
	//$(".loading").fadeOut();
	var i =0;
	var load = setInterval(function(){
		i++;
		$(".loading-box").text(i+"%").css("width", i*2+"px");
		if(i == 100){
			clearInterval(load);
			$(".loading").fadeOut();
			if($(window).width() > 800){
				$(".home-pic").css("opacity", 1);
				$(".home-pic").attr("src", $(".home-pic").attr("src"));
			}
			
		}
	}, 20);
}

function changePlaceHolder(){
	/*

<en><input required  type="text" name="name" value="" placeholder="Please enter your name"></en>

<en><input required  type="text" name="company" value="" placeholder="Please enter company name"></en>
<en><input required  type="tel" name="phone" value="" placeholder="Please enter your phone number"></en>
<en><input required  type="tel" name="tel" value="" placeholder="Please enter your home number"></en>
<en><input required  type="text" name="address" value="" placeholder="Please enter your address"></en>
<en><input required  type="email" name="email" value="" placeholder="Please enter your email"></en>
<en><textarea maxlength="200" required  rows="4" cols="50" name="msg" placeholder="Please enter the description of the problem(At most 200words)" form="usrform"></textarea></en>
	*/
}


function words_deal() {
	var curLength = $("#msg").val().length;
	if (curLength > 200) {
		var num = $("#msg").val().substr(0, 5);
		$("#msg").val(num);
	} else {
		$("#textCount").text(200-$("#msg").val().length+"/200");
	}
}

$(function(){
  var audio = document.getElementById('background_audio');

  document.getElementById('mute').addEventListener('click', function (e)
  {
      e = e || window.event;
      audio.muted = !audio.muted;
      e.preventDefault();
  }, false);
});

$(document).ready(function(){
  // scroll to element
    $('nav a[href^="#"]').on('click',function (e) {
      e.preventDefault();

      var target = this.hash;
      var $target = $(target);

      $('html, body').stop().animate({
          'scrollTop': $target.offset().top
      }, 900, 'swing');
    });
});